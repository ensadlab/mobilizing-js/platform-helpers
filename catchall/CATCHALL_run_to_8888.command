#!/bin/bash

echo
echo "****************************************"
echo "* Please type your password            *"
echo "****************************************"
echo

read -r -s secret

sudo -k


run=true
stop() {
  echo "$secret" | sudo -S nginx -s stop

  echo
  echo "nginx stopped"
  echo

  run=false
}

trap stop SIGINT SIGTERM SIGQUIT SIGHUP


# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)


echo "$secret" | sudo -S mkdir -p log
echo "$secret" | sudo -S nginx -p "$(pwd)" -c nginx_catchall_to_8888.conf

echo
echo "###################################"
echo "#                                 #"
echo "# Stop with                       #"
echo "#            sudo nginx -s stop   #"
echo "#                                 #"
echo "###################################"

while $run ; do
  sleep 1000
done

## Installation

You should run the script `init_dependencies.bash` in order to download and compile `dnsmasq` binary in `bin/dnsmasq`. This has to be done only once.

## Configuration

You may adapt `dnsmasq_local.conf`, `dnsmasq_public.conf`, `hosts` file to your needs.

Be sure to deactivate other DHCP services, like the one provided by the wifi router. It may give different IP to clients and may also point the clients to other DNS.

To be sure to use you own DNS locally, you can not rely on DHCP. you should change your network configuration. Even better create a new one. The IP of your network interface that will give IPs with DHCP must be 10.10.0.1 . It is usually an ethernet adapter connected to the local wifi.

You can still access the internet, usually with your public wifi. This should come first in order to send queries to the internet. For this one, keep DHCP, but to

For the *first* network interface, the one used to send queries, change the DNS settings, to use you local DNS. it should be 127.0.0.1.

## Run

### Private DNS server

Start local DNS with `DNS_run_public.command`. It will ask a password for sudo operations, and will keep running.

It will override your local domains (so you can work on your project), and forward other queries to online public DNS (so you can access the internet).

### Public DNS and DHCP server

Start DNS and DHCP with `DNS_run_public.command`. It will ask a password for sudo operations, and will keep running.

It will provide, through 10.10.0.1, a DHCP service to assign IP addresses to connected devices.

It will also provide a DNS service that will allow the connected devices to access you project. It will not forward other queries, so the connected devices can not access the internet.


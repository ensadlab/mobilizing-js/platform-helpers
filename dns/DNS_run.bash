#!/bin/bash

# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)


sudo_password="$(security find-generic-password -a "$(whoami)" -s 'Mobilizing-Sudo' -w)"

if (( $? != 0 )) ; then
  echo ''
  echo 'Password not accessible in Keychain'
  echo ''
  echo 'Please type password for Mobilizing-Sudo'
  read -s sudo_password
  echo ''
  echo 'Please type password again, to add it to keychain'
  security add-generic-password -a "$(whoami)" -s 'Mobilizing-Sudo' -w "$sudo_password"
fi

echo "$sudo_password" | sudo -S -k echo 'OK'

if (( $? != 0 )) ; then
    echo ''
    echo 'Bad password'
    exit 1
fi

clean_up() {
    echo 'clean up'
    echo "$sudo_password" | sudo -S -k bash -c "ps auwwx | grep -v $$ | grep ${configuration_file} | grep -v grep | grep -v sudo | awk '{print \$2}' | xargs kill"
}

configuration_file="$1"

# debug output
dnsmasq_command=(
  "$(pwd)/bin/dnsmasq"
  "--no-daemon"
  "--conf-file=${configuration_file}"
  "--quiet-dhcp"
  "--quiet-dhcp6"
  "--quiet-ra"
  "${@:2}"
)

# append arguments to command
# --interface=en7 to limit responses to specific network interface

# service
# command="$(pwd)/bin/dnsmasq --keep-in-foreground --conf-file=dnsmasq.conf"

echo "$sudo_password" | sudo -S -k rm -f dnsmasq.leases

mkdir -p log

while true ; do
    log_file="log/dnsmasq_run_$(date +"%Y-%m-%d_%H-%M-%S").log"
    date | tee "$log_file"

    clean_up
    echo 'start dnsmasq' | tee -a "$log_file"

    echo "$sudo_password" | sudo -S -k "${dnsmasq_command[@]}" 2>&1 | tee -a "$log_file"
    sleep 1
done
